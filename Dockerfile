# This Dockerfile is used to create an image for running Dalton Project integration tests

ARG dalton=registry.gitlab.com/daltonproject/docker/dalton/2020.1:latest
ARG lsdalton=registry.gitlab.com/daltonproject/docker/lsdalton/2020.0:latest
ARG pylsdalton=registry.gitlab.com/daltonproject/docker/lsdalton-omp/2020.0:latest

FROM ${dalton} as dalton_image
FROM ${lsdalton} as lsdalton_image
FROM ${pylsdalton} as pylsdalton_image

FROM registry.gitlab.com/daltonproject/docker/ubuntu:latest

COPY --from=dalton_image /dalton/ /dalton/
COPY --from=lsdalton_image /lsdalton/ /lsdalton/ 
COPY --from=pylsdalton_image /lsdalton/ /pylsdalton/ 

ENV PATH="/lsdalton/bin:/dalton/:${PATH}"
ENV PYTHONPATH="/pylsdalton/lib/python:${PYTHONPATH}"

WORKDIR /root/
