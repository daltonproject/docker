The Ubuntu file tree is

```
Ubuntu
+- Dalton
+- LSDalton
|  +- omp
+- DaltonProject
```
Each directory has a Dockerfile, that can be used to build a Docker image. 

The base Ubuntu build should be used for all subsequent Dalton, LSDalton and 
DaltonProject builds, and contains all dependencies needed to build Dalton 
and LSDalton. The base build typically does not need to be recompiled when 
updating either the Dalton or LSDalton images. Rather the Ubuntu base will only 
need to be rebuilt whenever Dalton and/or LSDalton requires an update of a
dependency. This could, for example, be when a newer version of CMake is needed.

Dalton and LSDalton can be built independently from each other on top of the 
Ubuntu base build, and the compiled versions of Dalton and LSDalton can be 
combined into a Dalton Project image.

Please note that the tag `latest` is reserved for releases. It should 
NEVER be used for development versions.

Example build times on a MacBook Pro with a 2.3 GHz 8-core Intel Core i9:
 - Ubuntu - 23 min
 - Dalton - 14 min
 - LSDalton - 15 min
 - Dalton Project - 1-2 min
