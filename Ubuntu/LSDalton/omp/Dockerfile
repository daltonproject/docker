
# Tag of LSDalton for OMP builds depends on branch/tag according to
#
#    docker build -t daltonproject/lsdalton:omp.v2020.0 .
#    docker push daltonproject/lsdalton:v2020.0
#
#    docker tag daltonproject/lsdalton:omp.v2020.0 daltonproject/lsdalton:omp.latest
#    docker push daltonproject/lsdalton:omp.latest
#
# for release tag v2020.0, or
#
#    docker build --build-arg branch="master" -t daltonproject/lsdalton:omp.master .
#
# get the git hash X through
#
#    docker run daltonproject/lsdalton:omp.master /bin/bash -c "more /lsdalton/GIT_HASH"   
#    
# then tag and push
#
#    docker tag daltonproject/lsdalton:omp.master daltonproject/lsdalton:omp.master.X
#    docker push daltonproject/lsdalton:omp.master.X
#    docker tag daltonproject/lsdalton:omp.master daltonproject/lsdalton:omp.master.latest
#    docker push daltonproject/lsdalton:omp.master.latest
#
# for the master branch with git hash X, or of course for any given branch/detached git hash/tag.
#
# The actual git hash of the build is available in the image under /lsdalton/GIT_HASH. 
#
# Please note that not all git hashes will be made into images
#
FROM registry.gitlab.com/daltonproject/docker/ubuntu:latest

ARG branch="v2020.0"

ENV FFLAGS "-Wno-argument-mismatch"
ENV MATH_ROOT="/usr/lib/x86_64-linux-gnu/"

WORKDIR /sources/
RUN git clone --recursive -b ${branch} --single-branch --depth=1 --shallow-submodules https://gitlab.com/dalton/lsdalton.git 

WORKDIR /sources/lsdalton/
RUN mkdir /lsdalton && git log -1 --pretty=%h > /lsdalton/GIT_HASH
RUN pip install -r requirements.txt && python /sources/lsdalton/setup --omp -DLAPACK_TYPE=SYSTEM_NATIVE -DENABLE_OPENRSP=ON -DENABLE_PYTHON_INTERFACE=ON -DENABLE_DEC=OFF -DENABLE_PCMSOLVER=OFF -DCMAKE_INSTALL_PREFIX=/lsdalton /sources/lsdalton/build
WORKDIR /sources/lsdalton/build/
RUN make install && rm -rf /sources

ENV PATH="/lsdalton/bin:${PATH}"
ENV LD_LIBRARY_PATH="/lsdalton/lib64:/lsdalton/lib:${LD_LIBRARY_PATH}"
ENV PYTHONPATH="/lsdalton/lib/python:${PYTHONPATH}"
ENV BASDIR="/lsdalton/share/lsdalton/basis/"

WORKDIR /root/
